//
//  Commons.swift
//  GradientLayout
//
//  Created by kashee on 16/06/18.
//  Copyright © 2018 Pro Retina. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func setBackGroundGradient(colorOne:UIColor,colorTwo:UIColor){
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = bounds
        gradientLayer.colors = [colorOne.cgColor,colorTwo.cgColor]
        gradientLayer.locations = [0.0,1.0]
        //        gradientLayer.startPoint = CGPoint(x: 1.0, y: 1.0)
        //        gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.0)
        layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func setDiagonalBackGroundGradient(colorOne:UIColor,colorTwo:UIColor){
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = bounds
        gradientLayer.colors = [colorOne.cgColor,colorTwo.cgColor]
        gradientLayer.locations = [0.0,1.0]
        gradientLayer.startPoint = CGPoint(x: 1.0, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.0)
        layer.insertSublayer(gradientLayer, at: 0)
    }
    
}

struct Colorss {
    
    static let announceRGB1 = UIColor(red: 112.0/255.0, green: 66.0/255.0, blue: 191.0/255.0, alpha: 0.80)
    static let announceRGB2 = UIColor(red: 48.0/255.0, green: 35.0/255.0, blue: 174/255.0, alpha: 0.80)
    
    static let pollRGB1 = UIColor(red: 23.0/255.0, green: 206.0/255.0, blue: 196.0/255.0, alpha: 1)
    static let pollRGB2 = UIColor(red: 8.0/255.0, green: 174.0/255.0, blue: 234.0/255.0, alpha: 1)
    
    static let leaveRGB1 = UIColor(red: 255.0/255.0, green: 131.0/255.0, blue: 8.0/255.0, alpha: 0.75)
    static let leaveRGB2 = UIColor(red: 253.0/255.0, green: 79.0/255.0, blue: 0/255.0, alpha: 0.75)
    
    static let complaintsRGB1 = UIColor(red: 237.0/255.0, green: 98.0/255.0, blue: 154.0/255.0, alpha: 0.95)
    static let complaintsRGB2 = UIColor(red: 200.0/255.0, green: 80.0/255.0, blue: 192/255.0, alpha: 0.95)
    
    static let headerRGB = UIColor(red: 33.0/255, green: 73.0/255, blue: 88.0/255, alpha: 1)
    
    static let titleRGB = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.60)
    
    
}

struct AnnnouncementColors {
    static let RGB1 = UIColor(red: 112.0/255.0, green: 66.0/255.0, blue: 191.0/255.0, alpha: 0.80)
    static let RGB2 = UIColor(red: 48.0/255.0, green: 35.0/255.0, blue: 174/255.0, alpha: 0.80)
}

struct PollColors {
    static let RGB1 = UIColor(red: 23.0/255.0, green: 206.0/255.0, blue: 196.0/255.0, alpha: 1)
    static let RGB2 = UIColor(red: 8.0/255.0, green: 174.0/255.0, blue: 234.0/255.0, alpha: 1)
}

struct LeaveColors {
    static let RGB1 = UIColor(red: 255.0/255.0, green: 131.0/255.0, blue: 8.0/255.0, alpha: 0.75)
    static let RGB2 = UIColor(red: 153.0/255.0, green: 79.0/255.0, blue: 0/255.0, alpha: 0.75)
}

struct ComplaintsColors {
    static let RGB1 = UIColor(red: 237.0/255.0, green: 98.0/255.0, blue: 154.0/255.0, alpha: 0.95)
    static let RGB2 = UIColor(red: 200.0/255.0, green: 80.0/255.0, blue: 192/255.0, alpha: 0.95)
}


extension UIViewController {
    func addInputAccessoryForTextFields(textFields: [UITextField], dismissable: Bool = true, previousNextable: Bool = false) {
        for (index, textField) in textFields.enumerated() {
            let toolbar: UIToolbar = UIToolbar()
            toolbar.sizeToFit()
            
            var items = [UIBarButtonItem]()
            if previousNextable {
                let previousButton = UIBarButtonItem(image: UIImage(named: "previous-arrow"), style: .plain, target: nil, action: nil)
                previousButton.width = 30
                if textField == textFields.first {
                    previousButton.isEnabled = false
                } else {
                    previousButton.target = textFields[index - 1]
                    previousButton.action = #selector(UITextField.becomeFirstResponder)
                }
                
                let nextButton = UIBarButtonItem(image: UIImage(named: "next-arrow"), style: .plain, target: nil, action: nil)
                nextButton.width = 30
                if textField == textFields.last {
                    nextButton.isEnabled = false
                } else {
                    nextButton.target = textFields[index + 1]
                    nextButton.action = #selector(UITextField.becomeFirstResponder)
                }
                items.append(contentsOf: [previousButton, nextButton])
            }
            
            let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: view, action: #selector(UIView.endEditing))
            items.append(contentsOf: [spacer, doneButton])
            
            
            toolbar.setItems(items, animated: false)
            textField.inputAccessoryView = toolbar
        }
    }
}


