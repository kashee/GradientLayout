//
//  DateSelectionViewController.swift
//  GradientLayout
//
//  Created by kashee on 22/06/18.
//  Copyright © 2018 Pro Retina. All rights reserved.
//

import UIKit

enum MyTheme {
    case light
    case dark
}

class DateSelectionViewController: UIViewController {

    var theme = MyTheme.dark
    
    let topView:UIView = {
        let view =  UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .orange
        return view
    }()
    
    
    let headerView:UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .orange
        return view
    }()
    
    let backButton:UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "backIcon"), for: .normal)
        
        button.addTarget(self, action: #selector(DateSelectionViewController.closeThisView), for: .touchUpInside)
        
        return button
    }()
    
    let startTextLavel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "New Leave Request"
        label.textAlignment = .left
        label.font = UIFont.boldSystemFont(ofSize: 26)
        label.textColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1)
        return label
    }()
    
    let calenderView: CalenderView = {
        let v=CalenderView(theme: MyTheme.light)
        v.translatesAutoresizingMaskIntoConstraints=false
        return v
    }()
    
    let continueButton:UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
//        button.backgroundColor = UIColor.init(red: 0/255, green: 180/255, blue: 226/255, alpha: 1)
        button.setTitle("Next", for: .normal)
        button.setTitleColor(UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 1), for: .normal)
        button.layer.cornerRadius = 25
        button.clipsToBounds = true
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 21)
//        button.addTarget(self, action: #selector(LoginWithEmailViewController.continueButtonTapped), for: .touchUpInside)
        
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.white
        
        theme = .light
        Style.themeLight()
        
        self.view.backgroundColor=Style.bgColor
        calenderView.changeTheme()
        
        addAutoLayout()
    }
    
    
    func addAutoLayout(){
        view.addSubview(topView)
        view.addSubview(headerView)
        topView.addSubview(backButton)
        headerView.addSubview(startTextLavel)
        view.addSubview(calenderView)
        view.addSubview(continueButton)
        
        
        topView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        topView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        topView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        topView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        if #available(iOS 11.0, *) {
            backButton.topAnchor.constraint(equalTo: topView.safeAreaLayoutGuide.topAnchor).isActive = true
        } else {
            // Fallback on earlier versions
            backButton.topAnchor.constraint(equalTo: topView.topAnchor).isActive = true
        }
        backButton.leadingAnchor.constraint(equalTo: topView.leadingAnchor).isActive = true
        backButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
        backButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
 
        headerView.topAnchor.constraint(equalTo: topView.bottomAnchor).isActive = true
        headerView.leadingAnchor.constraint(equalTo: topView.leadingAnchor).isActive = true
        //        headerView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor).isActive = true
        headerView.widthAnchor.constraint(equalTo: topView.widthAnchor).isActive = true
        headerView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        startTextLavel.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 20).isActive = true
        startTextLavel.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: 20).isActive = true
        startTextLavel.bottomAnchor.constraint(equalTo: headerView.bottomAnchor, constant: -20).isActive = true
        startTextLavel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        calenderView.topAnchor.constraint(equalTo: headerView.bottomAnchor, constant: 10).isActive=true
        calenderView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -12).isActive=true
        calenderView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 60).isActive=true
        calenderView.heightAnchor.constraint(equalToConstant: 365).isActive=true
        
        continueButton.topAnchor.constraint(equalTo: calenderView.bottomAnchor, constant: 10).isActive=true
        continueButton.leadingAnchor.constraint(equalTo: calenderView.leadingAnchor, constant: -12).isActive=true
        continueButton.widthAnchor.constraint(equalToConstant: 160).isActive = true
        continueButton.heightAnchor.constraint(equalToConstant: 50).isActive=true
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        calenderView.myCollectionView.collectionViewLayout.invalidateLayout()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        continueButton.setDiagonalBackGroundGradient(colorOne: UIColor.init(red: 0.0 / 255.0, green: 180.0 / 255.0, blue: 226.0 / 255.0, alpha: 1.0), colorTwo: UIColor.init(red: 2.0 / 255.0, green: 220.0 / 255.0, blue: 193.0 / 255.0, alpha: 1.0))
    }
    
    @objc func closeThisView(){
        navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

}
