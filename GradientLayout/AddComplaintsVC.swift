//
//  AddComplaintsVC.swift
//  GradientLayout
//
//  Created by kashee on 11/08/18.
//  Copyright © 2018 Pro Retina. All rights reserved.
//

import UIKit

class AddComplaintsVC: UIViewController {

    let topView:UIView = {
        let view =  UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .orange
        return view
    }()
    
    let containerView:UIView = {
        let view =  UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .green
        return view
    }()
    
    
    let backButton:UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "close_avatar"), for: .normal)
        
        //        button.addTarget(self, action: #selector(ViewPostViewController.closeThisView), for: .touchUpInside)
        return button
    }()
    
    let scrollView:UIScrollView = {
        let scroll = UIScrollView()
        scroll.translatesAutoresizingMaskIntoConstraints = false
        return scroll
    }()
    
    let titalLabel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Announcement Tital Low Attendance Hostel Block D"
        label.numberOfLines = 0
        label.sizeToFit()
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.textColor = UIColor(red: 33.0/255.0, green: 73.0/255.0, blue: 88.0/255.0, alpha: 1)
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
      
        addAutolayoutConstraints()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @objc func closeThisView(){
        navigationController?.popViewController(animated: true)
    }
    
    
    func addAutolayoutConstraints(){
        
        view.addSubview(topView)
        topView.addSubview(backButton)
        view.addSubview(scrollView)
        scrollView.addSubview(containerView)
        
        
        containerView.addSubview(titalLabel)
        topView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        topView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        topView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        topView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        backButton.topAnchor.constraint(equalTo: topView.topAnchor,constant:35).isActive = true
        backButton.leadingAnchor.constraint(equalTo: topView.leadingAnchor).isActive = true
        backButton.widthAnchor.constraint(equalToConstant: 70).isActive = true
        backButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        scrollView.topAnchor.constraint(equalTo: topView.bottomAnchor).isActive = true
        scrollView.leadingAnchor.constraint(equalTo: topView.leadingAnchor).isActive = true
        scrollView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        containerView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        containerView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor).isActive = true
        containerView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        containerView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
        containerView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor).isActive = true
        containerView.backgroundColor = .purple

        titalLabel.topAnchor.constraint(equalTo: containerView.topAnchor,constant:20).isActive = true
        titalLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor,constant:20).isActive = true
        titalLabel.widthAnchor.constraint(equalTo: view.widthAnchor,constant:-40).isActive = true
        //        titalLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 35).isActive = true
        titalLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
    }

}
