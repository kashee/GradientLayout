//
//  AddAnnouncementVC.swift
//  GradientLayout
//
//  Created by kashee on 27/08/18.
//  Copyright © 2018 Pro Retina. All rights reserved.
//

import UIKit

class AddAnnouncementVC: UIViewController, UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    var images = [String]()
    
    lazy var myCollectionView : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        let cv = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.isScrollEnabled = false
        cv.delegate = self
        cv.dataSource = self
        cv.register(CollectionViewCell.self, forCellWithReuseIdentifier: "CollectionCell")
        cv.register(AddButtonCollectionViewCell.self, forCellWithReuseIdentifier: "AddButtonCell")
        
        cv.backgroundColor = UIColor.clear
        return cv
    }()
    
    
    var selectedTF  : UITextField?
    let picker = UIPickerView()
    
    let topView:UIView = {
        let view =  UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .orange
        return view
    }()
    
    let contentView:UIView = {
        let view =  UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let backButton:UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "close_avatar"), for: .normal)
//        button.addTarget(self, action: #selector(AddAnnouncementVC.closeThisView), for: .touchUpInside)
        return button
    }()
    
    let scrollView:UIScrollView = {
        let scroll = UIScrollView()
        scroll.translatesAutoresizingMaskIntoConstraints = false
        return scroll
    }()
    
    lazy var titleLabel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.numberOfLines = 0
        label.sizeToFit()
        label.text = "New Announcement"
//        label.font = UIFont.sfDisplaySemibold(ofSize: 20)
//        label.textColor =  Colors.whiteTextRGB
        return label
    }()
    
    lazy var selectBlockLabel:UILabel = {
        let label = UILabel()
        self.setPropertiesForLB(lb : label, placeHolder : "Select Block")
        return label
    }()
    
    
    lazy var selectedBlockTextView:UITextView = {
        let textView = UITextView()
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.text = "Block-A, Golden, Block-D, Block-C, Block-H"
        textView.isScrollEnabled = false
        textView.isUserInteractionEnabled = false
//        textView.font = UIFont.sfDisplayRegular(ofSize: 15)
//        textView.textColor = Colors.headerRGB
        return textView
    }()
    
    lazy var leaveTypeLabel:UILabel = {
        let label = UILabel()
        self.setPropertiesForLB(lb : label, placeHolder : "Announcement Type")
        return label
    }()
    
    lazy var complaintTextField:UITextField = {
        let textField = UITextField()
        self.setPropertiesForTF(tf: textField,placeHolder: "Class Postponds")
        return textField
    }()
    
    lazy var photosLabel:UILabel = {
        let label = UILabel()
        self.setPropertiesForLB(lb : label, placeHolder : "Photos")
        return label
    }()
    
    lazy var selectImageView:UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "avatar"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
//        let tap = UITapGestureRecognizer(target: self, action: #selector( showActionSheetWithPIctureSelection))
//        imageView.isUserInteractionEnabled = true
//        imageView.addGestureRecognizer(tap)
        return imageView
    }()
    
    
    lazy var informToParentLabel:UILabel = {
        let label = UILabel()
        //        self.setPropertiesForLB(lb : label, placeHolder : "Inform Parent")
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Inform Parent"
        label.textAlignment = .right
        label.numberOfLines = 0
        label.sizeToFit()
//        label.font = UIFont.sfDisplayRegular(ofSize: 16)
        label.textColor =  UIColor(red: 33.0/255.0, green: 73.0/255.0, blue: 88.0/255.0, alpha: 0.75)
        return label
    }()
    
    let informParentSteper:UISwitch = {
        let stepper = UISwitch()
        stepper.translatesAutoresizingMaskIntoConstraints = false
        //        stepper.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        return stepper
    }()
    
    lazy var urgencyLabel:UILabel = {
        let label = UILabel()
        self.setPropertiesForLB(lb : label, placeHolder : "Urgency")
        return label
    }()
    
    
    let basicButton:UIButton = {
        let button = UIButton()
        button.layer.cornerRadius = 22
        button.setTitle( "Basic", for: .normal)
//        button.setButtonProperties(borderColor: Colors.selectedBdrClr, backgroundColor: Colors.selectedbgrClr, titleColor: Colors.whiteTextRGB)
        button.tag = 101
//        button.addTarget(self, action: #selector(AddComplaintViewController.didSelectUrgency), for: .touchUpInside)
        button.backgroundColor = .orange
        return button
    }()
    
    let mediumsButton:UIButton = {
        let button = UIButton()
        button.layer.cornerRadius = 22
        button.setTitle( "Medium", for: .normal)
//        button.setButtonProperties(borderColor: Colors.unSelectedBdrClr, backgroundColor: Colors.unSelectedbgrClr, titleColor: Colors.headerRGB)
        button.tag = 102
//        button.addTarget(self, action: #selector(AddComplaintViewController.didSelectUrgency), for: .touchUpInside)
        button.backgroundColor = .orange
        return button
    }()
    
    let criticalButton:UIButton = {
        let button = UIButton()
        button.layer.cornerRadius = 22
        button.setTitle( "Critical", for: .normal)
//        button.setButtonProperties(borderColor: Colors.unSelectedBdrClr, backgroundColor: Colors.unSelectedbgrClr, titleColor: Colors.headerRGB)
        button.tag = 103
//        button.addTarget(self, action: #selector(AddComplaintViewController.didSelectUrgency), for: .touchUpInside)
        button.backgroundColor = .orange
        return button
    }()
    
    lazy var descriptionLabel:UILabel = {
        let label = UILabel()
        self.setPropertiesForLB(lb : label, placeHolder : "Description")
        return label
    }()
    
    
    let descriptionField:UITextView = {
        let textField = UITextView()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = UIColor.init(red: 50.0/255, green: 60.0/255, blue: 71.0/255, alpha: 1)
        textField.backgroundColor = UIColor(red: 251/255, green: 252/255, blue: 253/255, alpha: 1)
//        textField.font = UIFont.sfDisplayRegular(ofSize: 14)
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor(red: 226/255, green: 226/255, blue: 229/255, alpha: 1).cgColor
        return textField
    }()
    
    
    let submitButton:UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Add Announcement", for: .normal)
//        button.setTitleColor(Colors.whiteTextRGB, for: .normal)
        button.layer.cornerRadius = 25
        button.clipsToBounds = true
//        button.titleLabel?.font = UIFont.sfDisplaySemibold(ofSize: 14)
        button.tag = 1
//        button.addTarget(self, action: #selector(AddComplaintViewController.didSubmitButtonTapped), for: .touchUpInside)
        return button
        
    }()
    
    var selectedImageIndex = 0
    
    var imagesArray = [Data]()
    var categoryId = ""
    
//    var categoryArray = [CategoryEntity]()
    var selectedPriority = 0
    var isAction = "false"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        images = ["avatar","avatar","avatar","avatar","avatar","avatar","avatar","avatar","avatar","avatar","avatar","avatar"]
        
        view.backgroundColor = .white
        addAutolaoutConstraints()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    

    // MARK: UICollectionViewDataSource
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.row == images.count {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddButtonCell", for: indexPath) as! AddButtonCollectionViewCell
            configureCollectionViewHeight()
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionCell", for: indexPath) as! CollectionViewCell

            cell.index = indexPath
            print("index: \(cell.index)")
            cell.delegate = self
            configureCollectionViewHeight()
            return cell
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:(collectionView.frame.width-10)/2,height:50)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        topView.setDiagonalBackGroundGradient(colorOne: UIColor.init(red: 112.0 / 255.0, green: 66.0 / 255.0, blue: 191.0 / 255.0, alpha: 0.95), colorTwo: UIColor.init(red: 48.0 / 255.0, green: 35.0 / 255.0, blue: 174.0 / 255.0, alpha: 0.95))
        
        submitButton.setDiagonalBackGroundGradient(colorOne: UIColor.init(red: 0.0 / 255.0, green: 180.0 / 255.0, blue: 226.0 / 255.0, alpha: 1.0), colorTwo: UIColor.init(red: 2.0 / 255.0, green: 220.0 / 255.0, blue: 193.0 / 255.0, alpha: 1.0))
        
    }
    
    func closeThisView() {
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ComplaintID"), object: nil, userInfo: ["isAction":isAction])
        self.navigationController?.popViewController(animated: true)
    }
    
    func setPropertiesForTF(tf : UITextField, placeHolder : String) {
        tf.translatesAutoresizingMaskIntoConstraints = false
//        tf.delegate = self
        tf.placeholder = placeHolder
        tf.textColor = UIColor.init(red: 50.0/255, green: 60.0/255, blue: 71.0/255, alpha: 1)
        tf.backgroundColor = UIColor(red: 251/255, green: 252/255, blue: 253/255, alpha: 1)
//        tf.font = UIFont.sfDisplayRegular(ofSize: 14)
        tf.borderStyle = .roundedRect
        
    }
    
    func setPropertiesForLB(lb : UILabel, placeHolder : String) {
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.text = placeHolder
        lb.textAlignment = .left
        lb.numberOfLines = 0
        lb.sizeToFit()
//        lb.font = UIFont.sfDisplayRegular(ofSize: 16)
        lb.textColor =  UIColor(red: 33.0/255.0, green: 73.0/255.0, blue: 88.0/255.0, alpha: 0.75)
        
    }
   
    func  addAutolaoutConstraints(){
        view.addSubview(topView)
        topView.addSubview(backButton)
        topView.addSubview(titleLabel)
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        
        contentView.addSubview(selectBlockLabel)
        contentView.addSubview(myCollectionView)
        contentView.addSubview(leaveTypeLabel)
        contentView.addSubview(complaintTextField)
        contentView.addSubview(urgencyLabel)
        contentView.addSubview(photosLabel)
        contentView.addSubview(selectImageView)
        contentView.addSubview(informToParentLabel)
        contentView.addSubview(informParentSteper)
        contentView.addSubview(mediumsButton)
        contentView.addSubview(criticalButton)
        contentView.addSubview(descriptionLabel)
        contentView.addSubview(descriptionField)
        contentView.addSubview(submitButton)
        
        topView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        topView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        topView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        topView.heightAnchor.constraint(equalToConstant: 145).isActive = true
        
        backButton.topAnchor.constraint(equalTo: topView.topAnchor,constant:35).isActive = true
        backButton.leadingAnchor.constraint(equalTo: topView.leadingAnchor).isActive = true
        backButton.widthAnchor.constraint(equalToConstant: 70).isActive = true
        backButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        titleLabel.bottomAnchor.constraint(equalTo: topView.bottomAnchor,constant:-8).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: topView.trailingAnchor, constant: -15).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: topView.leadingAnchor,constant:15).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        scrollView.topAnchor.constraint(equalTo: topView.bottomAnchor).isActive = true
        scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        scrollView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        contentView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        contentView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor).isActive = true
        contentView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        contentView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
        
        selectBlockLabel.topAnchor.constraint(equalTo: contentView.topAnchor,constant:20).isActive = true
        selectBlockLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor,constant:15).isActive = true
        selectBlockLabel.widthAnchor.constraint(equalTo: contentView.widthAnchor).isActive = true
        selectBlockLabel.heightAnchor.constraint(equalToConstant: 35).isActive = true
        
        myCollectionView.topAnchor.constraint(equalTo: selectBlockLabel.bottomAnchor,constant:1).isActive = true
        myCollectionView.leadingAnchor.constraint(equalTo: leaveTypeLabel.leadingAnchor).isActive = true
        myCollectionView.trailingAnchor.constraint(equalTo: topView.trailingAnchor, constant: -15).isActive = true
        myCollectionView.heightAnchor.constraint(greaterThanOrEqualToConstant: 100).isActive = true
        
        
        urgencyLabel.topAnchor.constraint(equalTo: myCollectionView.bottomAnchor,constant:15).isActive = true
        urgencyLabel.leadingAnchor.constraint(equalTo: leaveTypeLabel.leadingAnchor).isActive = true
        urgencyLabel.trailingAnchor.constraint(equalTo: topView.trailingAnchor, constant: -15).isActive = true
        urgencyLabel.heightAnchor.constraint(equalToConstant: 35).isActive = true
        
        let buttonStackView = UIStackView(arrangedSubviews: [basicButton,mediumsButton,criticalButton])
        buttonStackView.translatesAutoresizingMaskIntoConstraints = false
        buttonStackView.distribution = .fillEqually
        buttonStackView.spacing   = 10.0
        contentView.addSubview(buttonStackView)
        
        NSLayoutConstraint.activate([
            buttonStackView.topAnchor.constraint(equalTo: urgencyLabel.bottomAnchor,constant:5),
            buttonStackView.leadingAnchor.constraint(equalTo: urgencyLabel.leadingAnchor),
            buttonStackView.trailingAnchor.constraint(equalTo: urgencyLabel.trailingAnchor),
            buttonStackView.heightAnchor.constraint(equalToConstant: 44)
            ])
        
        
        leaveTypeLabel.topAnchor.constraint(equalTo: buttonStackView.bottomAnchor,constant:15).isActive = true
        leaveTypeLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor,constant:15).isActive = true
        leaveTypeLabel.widthAnchor.constraint(equalTo: contentView.widthAnchor).isActive = true
        leaveTypeLabel.heightAnchor.constraint(equalToConstant: 35).isActive = true
        
        complaintTextField.topAnchor.constraint(equalTo: leaveTypeLabel.bottomAnchor,constant:5).isActive = true
        complaintTextField.leadingAnchor.constraint(equalTo: leaveTypeLabel.leadingAnchor).isActive = true
        complaintTextField.trailingAnchor.constraint(equalTo: topView.trailingAnchor, constant: -15).isActive = true
        complaintTextField.heightAnchor.constraint(equalToConstant: 42).isActive = true
//        setSmallRighImageToTextField(textField: complaintTextField,image: "down-arrows")
        
        
        descriptionLabel.topAnchor.constraint(equalTo: complaintTextField.bottomAnchor, constant: 15).isActive = true
        descriptionLabel.leadingAnchor.constraint(equalTo: photosLabel.leadingAnchor).isActive = true
        descriptionLabel.trailingAnchor.constraint(equalTo: photosLabel.trailingAnchor).isActive = true
        descriptionLabel.heightAnchor.constraint(equalTo: photosLabel.heightAnchor).isActive = true
        
        descriptionField.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: 5).isActive = true
        descriptionField.leadingAnchor.constraint(equalTo: complaintTextField.leadingAnchor).isActive = true
        descriptionField.trailingAnchor.constraint(equalTo: complaintTextField.trailingAnchor).isActive = true
        descriptionField.heightAnchor.constraint(equalToConstant: 120).isActive = true
        
        
        
        photosLabel.topAnchor.constraint(equalTo: descriptionField.bottomAnchor, constant: 15).isActive = true
        photosLabel.leadingAnchor.constraint(equalTo: urgencyLabel.leadingAnchor).isActive = true
        photosLabel.widthAnchor.constraint(equalTo: urgencyLabel.widthAnchor, multiplier: 0.5).isActive = true
        photosLabel.heightAnchor.constraint(equalToConstant: 35).isActive = true
        
        selectImageView.topAnchor.constraint(equalTo: photosLabel.bottomAnchor, constant: 5).isActive = true
        selectImageView.leadingAnchor.constraint(equalTo: photosLabel.leadingAnchor).isActive = true
        selectImageView.widthAnchor.constraint(equalToConstant: 60).isActive = true
        selectImageView.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        
        informToParentLabel.topAnchor.constraint(equalTo: photosLabel.topAnchor).isActive = true
        informToParentLabel.trailingAnchor.constraint(equalTo: urgencyLabel.trailingAnchor).isActive = true
        informToParentLabel.widthAnchor.constraint(equalTo: urgencyLabel.widthAnchor, multiplier: 0.5).isActive = true
        informToParentLabel.heightAnchor.constraint(equalTo: photosLabel.heightAnchor).isActive = true
        
        informParentSteper.topAnchor.constraint(equalTo: informToParentLabel.bottomAnchor, constant: 5).isActive = true
        informParentSteper.trailingAnchor.constraint(equalTo: urgencyLabel.trailingAnchor).isActive = true
        //        informParentSteper.widthAnchor.constraint(equalToConstant: 40).isActive = true
        informParentSteper.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        
        
        submitButton.topAnchor.constraint(equalTo: selectImageView.bottomAnchor, constant: 20).isActive = true
        submitButton.leadingAnchor.constraint(equalTo: view.leadingAnchor,constant:32).isActive = true
        submitButton.trailingAnchor.constraint(equalTo: view.trailingAnchor,constant:-32).isActive = true
        submitButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        submitButton.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10).isActive = true
    }
    
    func configureCollectionViewHeight(){
        let height = myCollectionView.collectionViewLayout.collectionViewContentSize.height
        myCollectionView.heightAnchor.constraint(equalToConstant: height).isActive = true
        self.myCollectionView.layoutIfNeeded()
        self.scrollView.layoutIfNeeded()
    }

}


extension AddAnnouncementVC:DataCollectionProtocol{
    func passData(index: Int) {
        print("index value: \(index)")
    }
    
    func deleteData(index: Int) {
        images.remove(at: index)
        myCollectionView.reloadData()
    }
    
    
}
