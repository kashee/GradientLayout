//
//  KeyBoardManagerController.swift
//  GradientLayout
//
//  Created by kashee on 12/07/18.
//  Copyright © 2018 Pro Retina. All rights reserved.
//

import UIKit

class KeyBoardManagerController: UIViewController, UITextFieldDelegate {

    
    let usernameTextField:UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.text = "Shubham Vyash"
        textField.textColor = UIColor.init(red: 50.0/255, green: 60.0/255, blue: 71.0/255, alpha: 1)
        textField.borderStyle = .roundedRect
        return textField
    }()
    
    let studentIdTextField:UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.text = "Student ID"
        textField.textColor = UIColor.init(red: 50.0/255, green: 60.0/255, blue: 71.0/255, alpha: 1)
//        textField.font = UIFont.sfDisplayRegular(ofSize: 14)
        textField.borderStyle = .roundedRect
        return textField
    }()
    
    let emailTextField:UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.text = "email"
        textField.textColor = UIColor.init(red: 50.0/255, green: 60.0/255, blue: 71.0/255, alpha: 1)
//        textField.font = UIFont.sfDisplayRegular(ofSize: 14)
        textField.borderStyle = .roundedRect
        return textField
    }()
    
    
    let phoneTextField:UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.text = "Shubham Vyash"
        textField.textColor = UIColor.init(red: 50.0/255, green: 60.0/255, blue: 71.0/255, alpha: 1)
        textField.borderStyle = .roundedRect
        return textField
    }()
    
    let dobTextField:UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.text = "Student ID"
        textField.textColor = UIColor.init(red: 50.0/255, green: 60.0/255, blue: 71.0/255, alpha: 1)
        //        textField.font = UIFont.sfDisplayRegular(ofSize: 14)
        textField.borderStyle = .roundedRect
        return textField
    }()
    
    let joiningYearTextField:UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.text = "email"
        textField.textColor = UIColor.init(red: 50.0/255, green: 60.0/255, blue: 71.0/255, alpha: 1)
        //        textField.font = UIFont.sfDisplayRegular(ofSize: 14)
        textField.borderStyle = .roundedRect
        return textField
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        view.backgroundColor = .white
        view.addSubview(usernameTextField)
        view.addSubview(studentIdTextField)
        view.addSubview(emailTextField)
        
        view.addSubview(phoneTextField)
        view.addSubview(dobTextField)
        view.addSubview(joiningYearTextField)
        
        usernameTextField.delegate = self
        studentIdTextField.delegate = self
        emailTextField.delegate = self

        phoneTextField.delegate = self
        dobTextField.delegate = self
        joiningYearTextField.delegate = self
        
        
        usernameTextField.topAnchor.constraint(equalTo: view.topAnchor, constant: 60).isActive = true
        usernameTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 30).isActive = true
        usernameTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -30).isActive = true
        usernameTextField.heightAnchor.constraint(greaterThanOrEqualToConstant: 50).isActive = true
        
        studentIdTextField.topAnchor.constraint(equalTo: usernameTextField.bottomAnchor, constant: 60).isActive = true
        studentIdTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 30).isActive = true
        studentIdTextField.trailingAnchor.constraint(equalTo: usernameTextField.trailingAnchor).isActive = true
        studentIdTextField.heightAnchor.constraint(greaterThanOrEqualToConstant: 50).isActive = true
        
        emailTextField.topAnchor.constraint(equalTo: studentIdTextField.bottomAnchor, constant: 60).isActive = true
        emailTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 30).isActive = true
        emailTextField.trailingAnchor.constraint(equalTo: usernameTextField.trailingAnchor).isActive = true
        emailTextField.heightAnchor.constraint(greaterThanOrEqualToConstant: 50).isActive = true
        
        
        
        
        phoneTextField.topAnchor.constraint(equalTo: emailTextField.bottomAnchor, constant: 60).isActive = true
        phoneTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 30).isActive = true
        phoneTextField.trailingAnchor.constraint(equalTo: usernameTextField.trailingAnchor).isActive = true
        phoneTextField.heightAnchor.constraint(greaterThanOrEqualToConstant: 50).isActive = true
        
        dobTextField.topAnchor.constraint(equalTo: phoneTextField.bottomAnchor, constant: 60).isActive = true
        dobTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 30).isActive = true
        dobTextField.trailingAnchor.constraint(equalTo: usernameTextField.trailingAnchor).isActive = true
        dobTextField.heightAnchor.constraint(greaterThanOrEqualToConstant: 50).isActive = true
        
        joiningYearTextField.topAnchor.constraint(equalTo: dobTextField.bottomAnchor, constant: 60).isActive = true
        joiningYearTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 30).isActive = true
        joiningYearTextField.trailingAnchor.constraint(equalTo: usernameTextField.trailingAnchor).isActive = true
        joiningYearTextField.heightAnchor.constraint(greaterThanOrEqualToConstant: 50).isActive = true
        
        
        addInputAccessoryForTextFields(textFields: [usernameTextField, studentIdTextField, emailTextField, phoneTextField,
                                                    dobTextField, joiningYearTextField], dismissable: true, previousNextable: true)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

}
