//
//  ComplaintsListVC.swift
//  GradientLayout
//
//  Created by kashee on 09/08/18.
//  Copyright © 2018 Pro Retina. All rights reserved.
//

import UIKit

class ComplaintsListVC: UIViewController , UITableViewDelegate, UITableViewDataSource {
    
    
    let topView:UIView = {
        let view =  UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(red: 237/255, green: 98/255, blue: 154/255, alpha: 0.95)
        return view
    }()
    
    let scrollView:UIScrollView = {
        let scroll = UIScrollView()
        scroll.translatesAutoresizingMaskIntoConstraints = false
        scroll.backgroundColor = .red
        return scroll
    }()
    
    let headerView:UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .blue
        return view
    }()
    
    let backButton:UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "backIcon"), for: .normal)
        
        button.addTarget(self, action: #selector(LeaveListViewController.closeThisView), for: .touchUpInside)
        
        return button
    }()
    
    let AddNewButton:UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("New", for: .normal)
        button.setImage(#imageLiteral(resourceName: "add (1)"), for: .normal)
        button.imageView?.contentMode = .scaleAspectFit
        button.backgroundColor = UIColor(red: 139/255.0, green: 38/255.0, blue: 129/255.0, alpha: 0.80)
        button.layer.cornerRadius = 18
        button.contentVerticalAlignment = .fill
        button.contentHorizontalAlignment = .fill
        
        let spacing = CGFloat(17.0) // the amount of spacing to appear between image and title
        button.imageEdgeInsets = UIEdgeInsetsMake(9, 10, 9, spacing)
        button.titleEdgeInsets = UIEdgeInsetsMake(0, spacing, 0, 0)
        button.addTarget(self, action: #selector(moveToAddComplaints), for: .touchUpInside)
        
        return button
    }()
    
    lazy var tableView:UITableView = {
        let tv = UITableView(frame: CGRect.zero, style: UITableViewStyle.grouped)
        tv.delegate = self
        tv.dataSource = self
        tv.separatorStyle = .none
        tv.register(ComplaintListStudentTVCell.self, forCellReuseIdentifier: "CellId")
        tv.register(ComplaintListWardenTVCell.self, forCellReuseIdentifier: "ComplaintListWarden")
        tv.translatesAutoresizingMaskIntoConstraints = false
        //        tv.backgroundColor = .orange
        return tv
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor(red: 237/255, green: 98/255, blue: 154/255, alpha: 0.95)
        view.isOpaque = false
        
        self.tableView.estimatedSectionHeaderHeight = 80
        
        addAutoLayoutConstraints()
    }
    
    
    func addAutoLayoutConstraints(){
        view.addSubview(topView)
        topView.addSubview(backButton)
        topView.addSubview(AddNewButton)
        view.addSubview(tableView)
        
        topView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        topView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        topView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        topView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        if #available(iOS 11.0, *) {
            backButton.topAnchor.constraint(equalTo: topView.safeAreaLayoutGuide.topAnchor).isActive = true
        } else {
            // Fallback on earlier versions
            backButton.topAnchor.constraint(equalTo: topView.topAnchor).isActive = true
        }
        backButton.leadingAnchor.constraint(equalTo: topView.leadingAnchor).isActive = true
        backButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
        backButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        AddNewButton.topAnchor.constraint(equalTo: backButton.topAnchor).isActive = true
        AddNewButton.trailingAnchor.constraint(equalTo: topView.trailingAnchor,constant:-20).isActive = true
        AddNewButton.widthAnchor.constraint(equalToConstant: 93).isActive = true
        AddNewButton.heightAnchor.constraint(equalToConstant: 35).isActive = true
        
        tableView.topAnchor.constraint(equalTo: topView.bottomAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: topView.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: topView.trailingAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
    }
    
    @objc func closeThisView(){
        navigationController?.popViewController(animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let frame: CGRect = tableView.frame
        let headerView: UIView = UIView(frame: CGRect(x: 0, y: 0,width: frame.size.width, height:80))
        let headerButton: UIButton = UIButton(frame: CGRect(x: 0, y: 0,width: frame.size.width,height: 80))
        headerButton.setTitle("Leave Manager", for: .normal)
        headerButton.setTitleColor(UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1), for: .normal)
        headerButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 24)
        headerButton.backgroundColor = UIColor(red: 237/255, green: 98/255, blue: 154/255, alpha: 0.95)
        headerButton.titleEdgeInsets.left = -UIScreen.main.bounds.width + 200
        headerButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 300, bottom: 0, right: 5)
        headerButton.imageView?.contentMode = .scaleAspectFit
        headerButton.tag = section
        headerView.addSubview(headerButton)
        
        return headerView
        
        
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 80
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
//        let cell = tableView.dequeueReusableCell(withIdentifier: "CellId", for: indexPath) as! ComplaintListStudentTVCell
//        cell.selectionStyle = .none
//        return cell
        
        if indexPath.row % 2 == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellId", for: indexPath) as! ComplaintListStudentTVCell
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ComplaintListWarden", for: indexPath) as! ComplaintListWardenTVCell
            return cell
        }
        
        
        //        cell.cardView
        
    }
    
    // MARK: - Table view delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // etc
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row % 2 == 0{

            return 220
        }else{

            return 300
        }
        
//        return 220
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func moveToAddComplaints(){
        let complaintsVC = AddComplaintsVC()
        navigationController?.pushViewController(complaintsVC, animated: true)
    }
    
    
}
