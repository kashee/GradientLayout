//
//  RangeSelectorCalendarVC.swift
//  GradientLayout
//
//  Created by kashee on 28/07/18.
//  Copyright © 2018 Pro Retina. All rights reserved.
//

import UIKit
import Koyomi

class RangeSelectorCalendarVC: UIViewController,KoyomiDelegate {

    let topView:UIView = {
        let view =  UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .orange
        return view
    }()
    
    
    let headerView:UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .orange
        return view
    }()
    
    let backButton:UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "backIcon"), for: .normal)
        
        button.addTarget(self, action: #selector(RangeSelectorCalendarVC.closeThisView), for: .touchUpInside)
        
        return button
    }()
    
    let startTextLavel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "New Leave Request"
        label.textAlignment = .left
        label.font = UIFont.boldSystemFont(ofSize: 26)
        label.textColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1)
        return label
    }()
    
    let selectedDateLavel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Select the Dates"
        label.font = UIFont.boldSystemFont(ofSize: 22)
        label.textColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.60)
        return label
    }()
    
    let monthView:UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
//        view.backgroundColor = UIColor(red: 223/255.0, green: 223/255.0, blue: 223/255.0, alpha: 0.42)
        return view
    }()
    
    let calenderView: Koyomi = {
        let frame = CGRect(x: 10, y : 20, width: 250, height: 300)
        let koyomi = Koyomi(frame: frame, sectionSpace: 1.5, cellSpace: 0.5, inset: .zero, weekCellHeight: 25)
        koyomi.translatesAutoresizingMaskIntoConstraints=false
        koyomi.weeks = ("Su", "Mo", "Tu", "We", "Th", "Fr", "Sa")
        koyomi.isHiddenOtherMonth = true
        koyomi.inset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        koyomi.style = .standard
        koyomi.dayPosition = .center
        koyomi.selectionMode = .sequence(style: .semicircleEdge)
        koyomi.selectedStyleColor = UIColor(red: 255/255, green: 131/255, blue: 8/255, alpha: 1)
        return koyomi
    }()
    
    
    let lblName: UILabel = {
        let lbl=UILabel()
        lbl.text="July 2018"
        lbl.textColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.60)
        lbl.textAlignment = .center
        lbl.font=UIFont.boldSystemFont(ofSize: 16)
        lbl.translatesAutoresizingMaskIntoConstraints=false
        return lbl
    }()
    
    let btnRight: UIButton = {
        let btn=UIButton()
        btn.setTitle(">", for: .normal)
        btn.setTitleColor(UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.60), for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints=false
        btn.addTarget(self, action: #selector(btnLeftRightAction(sender:)), for: .touchUpInside)
        return btn
    }()
    
    let btnLeft: UIButton = {
        let btn=UIButton()
        btn.setTitle("<", for: .normal)
        btn.setTitleColor(UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.60), for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints=false
        btn.addTarget(self, action: #selector(btnLeftRightAction(sender:)), for: .touchUpInside)
        btn.setTitleColor(UIColor.lightGray, for: .disabled)
        return btn
    }()
    
    
    let continueButton:UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        //        button.backgroundColor = UIColor.init(red: 0/255, green: 180/255, blue: 226/255, alpha: 1)
        button.setTitle("Next", for: .normal)
        button.setTitleColor(UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 1), for: .normal)
        button.layer.cornerRadius = 25
        button.clipsToBounds = true
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 21)
        //        button.addTarget(self, action: #selector(LoginWithEmailViewController.continueButtonTapped), for: .touchUpInside)
        
        return button
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

         view.backgroundColor = .white
         addAutoLayout()
        calenderView.calendarDelegate = self
        calenderView.separatorColor = .white
        lblName.text = calenderView.currentDateString(withFormat: "MMM yyyy")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addAutoLayout(){
        
        view.addSubview(topView)
        view.addSubview(headerView)
        view.addSubview(calenderView)
        topView.addSubview(backButton)
        headerView.addSubview(startTextLavel)
        view.addSubview(continueButton)
        view.addSubview(selectedDateLavel)
        view.addSubview(monthView)
        
        topView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        topView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        topView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        topView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        if #available(iOS 11.0, *) {
            backButton.topAnchor.constraint(equalTo: topView.safeAreaLayoutGuide.topAnchor).isActive = true
        } else {
            // Fallback on earlier versions
            backButton.topAnchor.constraint(equalTo: topView.topAnchor).isActive = true
        }
        backButton.leadingAnchor.constraint(equalTo: topView.leadingAnchor).isActive = true
        backButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
        backButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        headerView.topAnchor.constraint(equalTo: topView.bottomAnchor).isActive = true
        headerView.leadingAnchor.constraint(equalTo: topView.leadingAnchor).isActive = true
        //        headerView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor).isActive = true
        headerView.widthAnchor.constraint(equalTo: topView.widthAnchor).isActive = true
        headerView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        startTextLavel.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 20).isActive = true
        startTextLavel.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: 20).isActive = true
        startTextLavel.bottomAnchor.constraint(equalTo: headerView.bottomAnchor, constant: -20).isActive = true
        startTextLavel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        
        
        
        selectedDateLavel.topAnchor.constraint(equalTo: headerView.bottomAnchor, constant: 10).isActive=true
        selectedDateLavel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -12).isActive=true
        selectedDateLavel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 60).isActive=true
        selectedDateLavel.heightAnchor.constraint(equalToConstant: 44).isActive=true
        
        monthView.topAnchor.constraint(equalTo: selectedDateLavel.bottomAnchor).isActive=true
        monthView.trailingAnchor.constraint(equalTo: selectedDateLavel.trailingAnchor).isActive=true
        monthView.leadingAnchor.constraint(equalTo: selectedDateLavel.leadingAnchor).isActive=true
        monthView.heightAnchor.constraint(equalToConstant: 65).isActive=true
        
        
        monthView.addSubview(lblName)
        lblName.topAnchor.constraint(equalTo: monthView.topAnchor).isActive=true
        lblName.centerXAnchor.constraint(equalTo: monthView.centerXAnchor).isActive=true
        lblName.widthAnchor.constraint(equalToConstant: 150).isActive=true
        lblName.heightAnchor.constraint(equalTo: monthView.heightAnchor).isActive=true
//        lblName.text="\(monthsArr[currentMonthIndex]) \(currentYear)"
        
        monthView.addSubview(btnRight)
        btnRight.topAnchor.constraint(equalTo: monthView.topAnchor).isActive=true
        btnRight.rightAnchor.constraint(equalTo: monthView.rightAnchor).isActive=true
        btnRight.widthAnchor.constraint(equalToConstant: 50).isActive=true
        btnRight.heightAnchor.constraint(equalTo: monthView.heightAnchor).isActive=true
        
        monthView.addSubview(btnLeft)
        btnLeft.topAnchor.constraint(equalTo: monthView.topAnchor).isActive=true
        btnLeft.leftAnchor.constraint(equalTo: monthView.leftAnchor).isActive=true
        btnLeft.widthAnchor.constraint(equalToConstant: 50).isActive=true
        btnLeft.heightAnchor.constraint(equalTo: monthView.heightAnchor).isActive=true
        
        
        
        calenderView.topAnchor.constraint(equalTo: monthView.bottomAnchor).isActive=true
        calenderView.trailingAnchor.constraint(equalTo: monthView.trailingAnchor).isActive=true
        calenderView.leadingAnchor.constraint(equalTo: monthView.leadingAnchor).isActive=true
        calenderView.heightAnchor.constraint(equalToConstant: 300).isActive=true
        
        continueButton.topAnchor.constraint(equalTo: calenderView.bottomAnchor, constant: 10).isActive=true
        continueButton.leadingAnchor.constraint(equalTo: calenderView.leadingAnchor, constant: -12).isActive=true
        continueButton.widthAnchor.constraint(equalToConstant: 160).isActive = true
        continueButton.heightAnchor.constraint(equalToConstant: 50).isActive=true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        continueButton.setDiagonalBackGroundGradient(colorOne: UIColor.init(red: 0.0 / 255.0, green: 180.0 / 255.0, blue: 226.0 / 255.0, alpha: 1.0), colorTwo: UIColor.init(red: 2.0 / 255.0, green: 220.0 / 255.0, blue: 193.0 / 255.0, alpha: 1.0))
    }
    
    @objc func closeThisView(){
        navigationController?.popViewController(animated: true)
    }
    
    
    @objc func btnLeftRightAction(sender: UIButton) {
        
        let month: MonthType = {
            if sender == btnRight {
                return .next
            } else {
                return .previous
            }
            
        }()
        
        calenderView.display(in: month)
//        lblName.text = calenderView.currentDateString()
        lblName.text = calenderView.currentDateString(withFormat: "MMM yyyy")

    }
    
    
    //　control date user selected.
    func koyomi(_ koyomi: Koyomi, shouldSelectDates date: Date?, to toDate: Date?, withPeriodLength length: Int) -> Bool {
        

        print("More than 90 days are invalid period. \(String(describing: date))")
        print("More than 90 days are invalid period.\(String(describing: toDate))")
        
        return true
    }
    
}
