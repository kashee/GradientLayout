//
//  SecondViewController.swift
//  GradientLayout
//
//  Created by kashee on 15/06/18.
//  Copyright © 2018 Pro Retina. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {

    var topView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.white
        return view
    }()
    
    var nameLabel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = Colorss.headerRGB
//        label.backgroundColor = .green
        label.textAlignment = .left
        label.font = UIFont.boldSystemFont(ofSize: 28)
        label.text = "Hi Karthik!"
        return label
    }()
    
    var wishLabel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = Colorss.titleRGB
        label.textAlignment = .left
        label.font = UIFont.boldSystemFont(ofSize: 18)
//        label.backgroundColor = .green
        label.text = "Good Morning..."
        return label
    }()
    

    lazy var myCollectionView : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
//        layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        layout.sectionInset = UIEdgeInsets(top: 0, left: 20, bottom: 10, right: 20)
//        layout.itemSize = CGSize(width: 111, height: 111)
        layout.minimumLineSpacing = 20.0
        layout.minimumInteritemSpacing = 20.0
        let cv = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.delegate = self
        cv.dataSource = self
        cv.register(CollectionCollectionViewCell.self, forCellWithReuseIdentifier: "CellId")
        cv.backgroundColor = .white
        return cv
    }()
    
    
    let homeColors = [AnnnouncementColors(),PollColors(),LeaveColors(),ComplaintsColors()] as [Any]

    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.white
        
        setAutoLayout()
    }
    
    
    func setAutoLayout(){
        view.addSubview(topView)
        topView.addSubview(nameLabel)
        topView.addSubview(wishLabel)
        view.addSubview(myCollectionView)
        
        topView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        topView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        topView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        topView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.22).isActive = true
        
        nameLabel.topAnchor.constraint(equalTo: topView.safeAreaLayoutGuide.topAnchor, constant: 30).isActive = true
        nameLabel.leadingAnchor.constraint(equalTo: topView.leadingAnchor,constant:20).isActive = true
        nameLabel.trailingAnchor.constraint(equalTo: topView.trailingAnchor).isActive = true
        nameLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        wishLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 5).isActive = true
        wishLabel.leadingAnchor.constraint(equalTo: topView.leadingAnchor,constant:20).isActive = true
        wishLabel.trailingAnchor.constraint(equalTo: topView.trailingAnchor).isActive = true
        wishLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        myCollectionView.topAnchor.constraint(equalTo: topView.bottomAnchor,constant:5).isActive = true
        myCollectionView.leadingAnchor.constraint(equalTo: topView.leadingAnchor).isActive = true
        myCollectionView.trailingAnchor.constraint(equalTo: topView.trailingAnchor).isActive = true
        myCollectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellId", for: indexPath as IndexPath) as! CollectionCollectionViewCell
//        cell.backgroundColor = UIColor.green
        if indexPath.row == 0{
            cell.clor1 = Colorss.announceRGB1
            cell.clor2 = Colorss.announceRGB2
        }else if indexPath.row == 1{
            cell.clor1 = Colorss.pollRGB1
            cell.clor2 = Colorss.pollRGB2
        }else if indexPath.row == 2{
            cell.clor1 = Colorss.leaveRGB1
            cell.clor2 = Colorss.leaveRGB2
        }else if indexPath.row == 3{
            cell.clor1 = Colorss.complaintsRGB1
            cell.clor2 = Colorss.complaintsRGB2
        }

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:(self.myCollectionView.frame.width-60)/2,height:(self.myCollectionView.frame.height)/4)
    }
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
