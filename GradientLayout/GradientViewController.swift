//
//  GradientViewController.swift
//  GradientLayout
//
//  Created by kashee on 07/07/18.
//  Copyright © 2018 Pro Retina. All rights reserved.
//

import UIKit

class GradientViewController: UIViewController {

    let centerView:UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        view.addSubview(centerView)
        
        centerView.topAnchor.constraint(equalTo: view.topAnchor,constant:100).isActive = true
        centerView.leadingAnchor.constraint(equalTo: view.leadingAnchor,constant:30).isActive = true
        centerView.trailingAnchor.constraint(equalTo: view.trailingAnchor,constant:-30).isActive = true
        centerView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        

    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        centerView.setBackGroundGradient(colorOne: AnnnouncementColors.RGB1, colorTwo: AnnnouncementColors.RGB2)
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
