//
//  ViewController.swift
//  GradientLayout
//
//  Created by kashee on 11/06/18.
//  Copyright © 2018 Pro Retina. All rights reserved.
//

import UIKit


extension CAGradientLayer {
    
    func graphViewBackgroundColor() -> CAGradientLayer {
        
        let topColor = UIColor.init(red: 0.0 / 255.0, green: 180.0 / 255.0, blue: 226.0 / 255.0, alpha: 1.0)
        let bottomColor = UIColor.init(red: 2.0 / 255.0, green: 220.0 / 255.0, blue: 193.0 / 255.0, alpha: 1.0)
        
        let gradientColors: [CGColor] = [topColor.cgColor, bottomColor.cgColor]
        let gradientLocations: [Float] = [0.0, 1.0]
        
        let gradientLayer: CAGradientLayer = CAGradientLayer()
        gradientLayer.colors = gradientColors
        gradientLayer.startPoint = CGPoint(x: 1.0, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.0)
        gradientLayer.locations = gradientLocations as [NSNumber]
        
        return gradientLayer
    }
}



class ViewController: UIViewController {

    var  gradientLayer: CAGradientLayer!
    var imageView = UIImageView()
    var button2 = UIButton()
    
    
    let leaveListButton:UIButton = {
        let button = UIButton()
                button.translatesAutoresizingMaskIntoConstraints = false
                button.setTitle("Leave List", for: .normal)
                button.setTitleColor(UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 1), for: .normal)
                button.layer.cornerRadius = 25
//                button.titleLabel?.font = UIFont.sfDisplaySemibold(ofSize: 17)
        
//                self.view.applyGradient([UIColor.yellowColor(), UIColor.blueColor(), UIColor.redColor()], locations: [0.0, 0.5, 1.0])
        
//                button.setDiagonalBackGroundGradient(colorOne: UIColor.init(red: 2.0 / 255.0, green: 220.0 / 255.0, blue: 193.0 / 255.0, alpha: 1.0), colorTwo: UIColor.init(red: 0.0 / 255.0, green: 180.0 / 255.0, blue: 226.0 / 255.0, alpha: 1.0))

        button.addTarget(self, action: #selector(ViewController.movetoLeaveList), for: .touchUpInside)
        
        return button
    }()
    
    
    
    
    let viewLeaveButton:UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("View Leave", for: .normal)
        button.setTitleColor(UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 1), for: .normal)
        button.layer.cornerRadius = 25
        button.addTarget(self, action: #selector(ViewController.movetoViewLeave), for: .touchUpInside)
        
        return button
    }()
    
    let selectDateButton:UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Select Date", for: .normal)
        button.setTitleColor(UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 1), for: .normal)
        button.layer.cornerRadius = 25
        button.addTarget(self, action: #selector(ViewController.movetoVselectDate), for: .touchUpInside)
        
        return button
    }()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        button2.frame = CGRect(x: 60, y: 100, width: 100, height: 45)
        button2.setTitle("Hi kashee", for: .normal)
        button2.setTitleColor(UIColor.red, for: .normal)
        button2.setDiagonalBackGroundGradient(colorOne: UIColor.blue, colorTwo: UIColor.orange)
        view.addSubview(button2)
        
        setAutoLayout()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
//        setBackGroundImage()
//        createGradientLayer()
        self.view.backgroundColor = UIColor.white
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
//        let backgroundColor = CAGradientLayer().graphViewBackgroundColor()
//        backgroundColor.frame = self.leaveListButton.frame
//        self.leaveListButton.layer.addSublayer(backgroundColor)
        
        leaveListButton.setBackGroundGradient(colorOne: UIColor.init(red: 0.0 / 255.0, green: 180.0 / 255.0, blue: 226.0 / 255.0, alpha: 1.0), colorTwo: UIColor.init(red: 2.0 / 255.0, green: 220.0 / 255.0, blue: 193.0 / 255.0, alpha: 1.0))
        
        viewLeaveButton.setBackGroundGradient(colorOne: UIColor.init(red: 0.0 / 255.0, green: 180.0 / 255.0, blue: 226.0 / 255.0, alpha: 1.0), colorTwo: UIColor.init(red: 2.0 / 255.0, green: 220.0 / 255.0, blue: 193.0 / 255.0, alpha: 1.0))
        
        selectDateButton.setBackGroundGradient(colorOne: UIColor.init(red: 0.0 / 255.0, green: 180.0 / 255.0, blue: 226.0 / 255.0, alpha: 1.0), colorTwo: UIColor.init(red: 2.0 / 255.0, green: 220.0 / 255.0, blue: 193.0 / 255.0, alpha: 1.0))
        
    }
    
    
    func setAutoLayout(){
        
        view.addSubview(leaveListButton)
        view.addSubview(viewLeaveButton)
        view.addSubview(selectDateButton)
        leaveListButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 300).isActive = true
        leaveListButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: -31).isActive = true
        leaveListButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 31).isActive = true
        leaveListButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        viewLeaveButton.topAnchor.constraint(equalTo: leaveListButton.bottomAnchor, constant: 20).isActive = true
        viewLeaveButton.leadingAnchor.constraint(equalTo: leaveListButton.leadingAnchor).isActive = true
        viewLeaveButton.trailingAnchor.constraint(equalTo: leaveListButton.trailingAnchor).isActive = true
        viewLeaveButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        selectDateButton.topAnchor.constraint(equalTo: viewLeaveButton.bottomAnchor, constant: 20).isActive = true
        selectDateButton.leadingAnchor.constraint(equalTo: viewLeaveButton.leadingAnchor).isActive = true
        selectDateButton.trailingAnchor.constraint(equalTo: viewLeaveButton.trailingAnchor).isActive = true
        selectDateButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
    }
    
    
    @objc func movetoLeaveList(){
//       let nextVC = ViewLeaveViewController()
//       let nextVC = SecondViewController()
//       let nextVC = LeaveListViewController()
        let nextVC = AddAnnouncementVC()
        navigationController?.pushViewController(nextVC, animated: true)
    }
    
    @objc func movetoViewLeave(){
        let nextVC = DateSelectionViewController()
        navigationController?.pushViewController(nextVC, animated: true)
    }
    
    @objc func movetoVselectDate(){
        
//        let nextVC = KeyBoardManagerController()
        let nextVC = RangeSelectorCalendarVC()
        navigationController?.pushViewController(nextVC, animated: true)

    }
    
    
    func createGradientLayer() {

        let colors = Colors()
        colors.gl.frame = self.view.bounds
//        self.view.layer.addSublayer(colors.gl)
        self.view.layer.insertSublayer(colors.gl, at: 0)
//        self.view.backgroundColor = UIColor.darkGray
    }
    
    func setBackGroundImage(){
        imageView.frame = self.view.bounds
        self.view.addSubview(imageView)
//        self.view.bringSubview(toFront: imageView)
//        self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Splash")!)
//        imageView.image = UIImage(named: "Splash")
//        imageView.contentMode = .scaleAspectFill
        imageView.image = UIImage(named: "circlesBg")
        imageView.contentMode = .scaleAspectFit
        
//        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "circlesBg")!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}



