//
//  ViewLeaveViewController.swift
//  GradientLayout
//
//  Created by kashee on 20/06/18.
//  Copyright © 2018 Pro Retina. All rights reserved.
//

import UIKit

class ViewLeaveViewController: UIViewController {

    
    let topView:UIView = {
        let view =  UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .orange
        return view
    }()
    
    let scrollView:UIScrollView = {
        let scroll = UIScrollView()
        scroll.translatesAutoresizingMaskIntoConstraints = false
        scroll.backgroundColor = .white
        return scroll
    }()
    
    let headerView:UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .orange
        return view
    }()
    
    let startDateLavel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "21st Jan"
        label.font = UIFont.boldSystemFont(ofSize: 26)
        label.textColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1)
        return label
    }()
    
    let startTextLavel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Start Date"
        label.textColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1)
        return label
    }()
    
    let endDateLavel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "23rd Jan"
        label.textAlignment = .left
        label.font = UIFont.boldSystemFont(ofSize: 26)
        label.textColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1)
        return label
    }()
    
    let endTextLavel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "End Date        "
        label.textAlignment = .left
        label.textColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1)
        return label
    }()
    
    let backButton:UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "backIcon"), for: .normal)
        
        button.addTarget(self, action: #selector(ViewLeaveViewController.closeThisView), for: .touchUpInside)
        
        return button
    }()
    
    let statusLabel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Status"
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textColor = UIColor(red: 33.0/255.0, green: 73.0/255.0, blue: 88.0/255.0, alpha: 0.75)
        return label
    }()
    
    let statusButton:UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("✓ Approved", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.setTitleColor(UIColor(red: 45.0/255.0, green: 199.0/255.0, blue: 109.0/255.0, alpha: 1), for: .normal)
        button.backgroundColor = UIColor(red: 208.0/255.0, green: 255.0/255.0, blue: 227.0/255.0, alpha: 1)
        button.layer.cornerRadius = 18
        return button
    }()
    
    let leaveTypeLabel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Leave Type"
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textColor = UIColor(red: 33.0/255.0, green: 73.0/255.0, blue: 88.0/255.0, alpha: 0.75)
        return label
    }()
    
    let leaveTypeTextField:UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.text = "Hospitalization Leave"
        textField.font = UIFont.boldSystemFont(ofSize: 16)
        textField.textColor = UIColor(red: 33.0/255.0, green: 73.0/255.0, blue: 88.0/255.0, alpha: 1)
        return textField
    }()
    
    let notesLabel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Notes:"
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textColor = UIColor(red: 33.0/255.0, green: 73.0/255.0, blue: 88.0/255.0, alpha: 0.75)
        return label
    }()
    
    let notesTextView:UITextView = {
        let textView = UITextView()
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.text = "“My eye will be operated and will be admitted in the hospital for 4 days. My eye will be operated and will be admitted in the hospital for 4 days.”"
        textView.textColor = .black
        textView.font = UIFont.boldSystemFont(ofSize: 16)
        textView.textColor = UIColor(red: 33.0/255.0, green: 73.0/255.0, blue: 88.0/255.0, alpha: 1)
        return textView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.orange
        view.isOpaque = false
        addAutoLayoutConstraints()
        
        // Do any additional setup after loading the view.
    }
    
    
    
    func addAutoLayoutConstraints(){
        view.addSubview(topView)
        view.addSubview(scrollView)
        topView.addSubview(backButton)
        scrollView.addSubview(headerView)
        scrollView.addSubview(statusLabel)
        scrollView.addSubview(statusButton)
        scrollView.addSubview(leaveTypeLabel)
        scrollView.addSubview(leaveTypeTextField)
        scrollView.addSubview(notesLabel)
        scrollView.addSubview(notesTextView)
        
        headerView.addSubview(startTextLavel)
        headerView.addSubview(startDateLavel)
        headerView.addSubview(endTextLavel)
        headerView.addSubview(endDateLavel)
        
        
        topView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        topView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        topView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        topView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        if #available(iOS 11.0, *) {
            backButton.topAnchor.constraint(equalTo: topView.safeAreaLayoutGuide.topAnchor).isActive = true
        } else {
            // Fallback on earlier versions
            backButton.topAnchor.constraint(equalTo: topView.topAnchor).isActive = true
        }
        backButton.leadingAnchor.constraint(equalTo: topView.leadingAnchor).isActive = true
        backButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
        backButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        scrollView.topAnchor.constraint(equalTo: topView.bottomAnchor).isActive = true
        scrollView.leadingAnchor.constraint(equalTo: topView.leadingAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: topView.trailingAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        headerView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        headerView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor).isActive = true
//        headerView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor).isActive = true
        headerView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        headerView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        startTextLavel.leadingAnchor.constraint(equalTo: headerView.leadingAnchor,constant:20).isActive = true
        startTextLavel.bottomAnchor.constraint(equalTo: headerView.bottomAnchor,constant:-8).isActive = true
        startTextLavel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        startTextLavel.widthAnchor.constraint(equalTo: headerView.widthAnchor, multiplier: 0.4)
        
        startDateLavel.leadingAnchor.constraint(equalTo: startTextLavel.leadingAnchor).isActive = true
        startDateLavel.bottomAnchor.constraint(equalTo: startTextLavel.topAnchor).isActive = true
        startDateLavel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        startDateLavel.widthAnchor.constraint(equalTo: headerView.widthAnchor, multiplier: 0.4)
        
        
        endTextLavel.trailingAnchor.constraint(equalTo: headerView.trailingAnchor,constant:-20).isActive = true
        endTextLavel.bottomAnchor.constraint(equalTo: headerView.bottomAnchor,constant:-8).isActive = true
        endTextLavel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        endTextLavel.widthAnchor.constraint(equalTo: headerView.widthAnchor, multiplier: 0.5)
//        endTextLavel.backgroundColor = .green
        
        endDateLavel.trailingAnchor.constraint(equalTo: endTextLavel.trailingAnchor).isActive = true
        endDateLavel.bottomAnchor.constraint(equalTo: endTextLavel.topAnchor).isActive = true
        endDateLavel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        endDateLavel.widthAnchor.constraint(equalTo: headerView.widthAnchor, multiplier: 0.4)
        
        
        
        statusLabel.topAnchor.constraint(equalTo: headerView.bottomAnchor,constant:30).isActive = true
        statusLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor,constant:30).isActive = true
        statusLabel.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        statusLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        statusButton.topAnchor.constraint(equalTo: statusLabel.bottomAnchor,constant:2).isActive = true
        statusButton.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor,constant:30).isActive = true
        statusButton.widthAnchor.constraint(equalToConstant: 140).isActive = true
        statusButton.heightAnchor.constraint(equalToConstant: 38).isActive = true
        
        leaveTypeLabel.topAnchor.constraint(equalTo: statusButton.bottomAnchor,constant:30).isActive = true
        leaveTypeLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor,constant:30).isActive = true
        leaveTypeLabel.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        leaveTypeLabel.heightAnchor.constraint(equalToConstant: 35).isActive = true
        
        leaveTypeTextField.topAnchor.constraint(equalTo: leaveTypeLabel.bottomAnchor,constant:1).isActive = true
        leaveTypeTextField.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor,constant:35).isActive = true
        leaveTypeTextField.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        leaveTypeTextField.heightAnchor.constraint(equalToConstant: 35).isActive = true
        
        notesLabel.topAnchor.constraint(equalTo: leaveTypeTextField.bottomAnchor,constant:30).isActive = true
        notesLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor,constant:30).isActive = true
        notesLabel.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        notesLabel.heightAnchor.constraint(equalToConstant: 35).isActive = true
        
        notesTextView.topAnchor.constraint(equalTo: notesLabel.bottomAnchor,constant:5).isActive = true
        notesTextView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor,constant:35).isActive = true
        notesTextView.widthAnchor.constraint(equalTo: view.widthAnchor,constant:-50).isActive = true
        notesTextView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        
        
    }
    
    
   @objc func closeThisView(){
        navigationController?.popViewController(animated: true)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
