//
//  SplashViewController.swift
//  GradientLayout
//
//  Created by kashee on 12/06/18.
//  Copyright © 2018 Pro Retina. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    var  gradientLayer: CAGradientLayer!
    var imageView = UIImageView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        setBackGroundImage()
        createGradientLayer()
        
        perform(#selector(showNavControler), with: nil, afterDelay: 3)
    }

    
    func createGradientLayer() {
        
        let colors = Colors()
        colors.gl.frame = self.view.bounds
        self.view.layer.insertSublayer(colors.gl, at: 0)
    }
    
    func setBackGroundImage(){
        imageView.frame = self.view.bounds
        self.view.addSubview(imageView)
        imageView.image = UIImage(named: "circlesBg")
        imageView.contentMode = .scaleAspectFit
        
    }
    
    @objc func showNavControler(){
        performSegue(withIdentifier: "SplashScreen", sender: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
