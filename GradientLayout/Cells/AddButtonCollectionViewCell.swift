//
//  AddButtonCollectionViewCell.swift
//  GradientLayout
//
//  Created by kashee on 29/08/18.
//  Copyright © 2018 Pro Retina. All rights reserved.
//

import UIKit

class AddButtonCollectionViewCell: UICollectionViewCell {
    
    
    lazy var addItemButton:UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("New", for: .normal)
        button.setImage(#imageLiteral(resourceName: "path4"), for: .normal)
        button.imageView?.contentMode = .scaleAspectFit
        button.backgroundColor = UIColor(red: 0/255.0, green: 182/255.0, blue: 225/255.0, alpha: 0.80)
        button.layer.cornerRadius = 18
        button.contentVerticalAlignment = .fill
        button.contentHorizontalAlignment = .fill
        
        let spacing = CGFloat(17.0) // the amount of spacing to appear between image and title
        button.imageEdgeInsets = UIEdgeInsetsMake(9, 10, 9, spacing)
        button.titleEdgeInsets = UIEdgeInsetsMake(0, spacing, 0, 0)
//        button.addTarget(self, action: #selector(didAddButtonPressed), for: .touchUpInside)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
//        self.backgroundColor = .green
        addAutoLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func addAutoLayout(){
        self.addSubview(addItemButton)
        
        addItemButton.topAnchor.constraint(equalTo: self.topAnchor,constant:2).isActive = true
        addItemButton.leadingAnchor.constraint(equalTo: self.leadingAnchor,constant:2).isActive = true
        addItemButton.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -2).isActive = true
        addItemButton.bottomAnchor.constraint(equalTo: self.bottomAnchor,constant:-2).isActive = true
        
    }
}
