//
//  CollectionViewCell.swift
//  GradientLayout
//
//  Created by kashee on 27/08/18.
//  Copyright © 2018 Pro Retina. All rights reserved.
//

import UIKit

protocol DataCollectionProtocol {
    func passData(index:Int)
    func deleteData(index:Int)
}

class CollectionViewCell: UICollectionViewCell {
    
    var delegate: DataCollectionProtocol?
    var index: IndexPath?
    
    let label:UILabel = {
        let labl = UILabel()
        labl.translatesAutoresizingMaskIntoConstraints = false
        labl.text = "Some Name"
        return labl
    }()
    
    lazy var attachmentImages:UIButton = {
        let imageView = UIButton()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.setImage(#imageLiteral(resourceName: "avatar"), for: .normal)
        imageView.imageView?.contentMode = .scaleAspectFit
        imageView.addTarget(self, action: #selector(deleteCell), for: .touchUpInside)
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = .green
        addAutoLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func addAutoLayout(){
        self.addSubview(label)
        self.addSubview(attachmentImages)
        
        label.topAnchor.constraint(equalTo: self.topAnchor,constant:10).isActive = true
        label.leadingAnchor.constraint(equalTo: self.leadingAnchor,constant:5).isActive = true
        label.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.7).isActive = true
        label.bottomAnchor.constraint(equalTo: self.bottomAnchor,constant:-10).isActive = true
        
        attachmentImages.topAnchor.constraint(equalTo: label.topAnchor).isActive = true
        attachmentImages.leadingAnchor.constraint(equalTo: label.trailingAnchor,constant:2).isActive = true
        attachmentImages.widthAnchor.constraint(equalToConstant: 20).isActive = true
        attachmentImages.heightAnchor.constraint(equalToConstant: 20).isActive = true
    }
    
    @objc func deleteCell(_ sender:Any){
        delegate?.deleteData(index: (index?.row)!)
    }
}
