//
//  LeaveListTableViewCell.swift
//  GradientLayout
//
//  Created by kashee on 20/06/18.
//  Copyright © 2018 Pro Retina. All rights reserved.
//

import UIKit

class LeaveListTableViewCell: UITableViewCell {

    let cardView:UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.init(red: 255.0 / 255.0, green: 255.0 / 255.0, blue: 255.0 / 255.0, alpha: 1.0)
        view.layer.cornerRadius = 5
        return view
    }()
    
    let dateLabel:UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "21st jan - 28th jan 2018"
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.textColor = UIColor(red: 33.0/255.0, green: 73.0/255.0, blue: 88.0/255.0, alpha: 1)
        return label
    }()
    
    let leaveLogoImageView:UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "icons8Hospital48"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        //        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let titleLabel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Hospitalization Leave"
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.60)
        return label
    }()
    
    let statusLabel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Awaiting Parent Approval!"
        label.textColor = .black
        label.textAlignment = .right
        label.numberOfLines = 0
        label.sizeToFit()
        label.font = UIFont.boldSystemFont(ofSize: 11)
        label.textColor = UIColor(red: 245/255.0, green: 166/255.0, blue: 35/255.0, alpha: 0.75)
        return label
    }()
    
    let statusButton:UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(#imageLiteral(resourceName: "noun1763918Cc"), for: .normal)
        button.imageView?.contentMode = .scaleAspectFit
        button.contentVerticalAlignment = .fill
        button.contentHorizontalAlignment = .fill
        button.imageEdgeInsets = UIEdgeInsetsMake(7, 10, 7, 10)
//        button.imageEdgeInsets = UIEdgeInsetsMake(2, 5, 2, 5)
        button.backgroundColor = UIColor(red: 245/255.0, green: 166/255.0, blue: 35/255.0, alpha: 0.20)
        button.layer.cornerRadius = 12
        return button
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = UIColor(red: 10/255, green: 178/255, blue: 230/255, alpha: 0.0)
        addAutoLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
//          shadowforView(button: cardView, color: .lightGray)
        shadowforView(button: cardView, color: UIColor(red: 37/255, green: 38/255, blue: 94/255, alpha: 0.1))
    }
    
    
    func addAutoLayout(){
        self.addSubview(cardView)
        cardView.addSubview(dateLabel)
        cardView.addSubview(titleLabel)
        cardView.addSubview(statusLabel)
        cardView.addSubview(statusButton)
        cardView.addSubview(leaveLogoImageView)
        
        cardView.topAnchor.constraint(equalTo: self.topAnchor,constant:10).isActive = true
        cardView.leadingAnchor.constraint(equalTo: self.leadingAnchor,constant:10).isActive = true
        cardView.trailingAnchor.constraint(equalTo: self.trailingAnchor,constant:-10).isActive = true
        cardView.bottomAnchor.constraint(equalTo: self.bottomAnchor,constant:-10).isActive = true
        
        
        statusButton.topAnchor.constraint(equalTo: cardView.topAnchor, constant: 15).isActive = true
        statusButton.trailingAnchor.constraint(equalTo: cardView.trailingAnchor, constant: -12).isActive = true
        statusButton.widthAnchor.constraint(equalToConstant: 42).isActive = true
        statusButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        statusLabel.topAnchor.constraint(equalTo: statusButton.bottomAnchor, constant: 5).isActive = true
        statusLabel.trailingAnchor.constraint(equalTo: statusButton.trailingAnchor).isActive = true
        statusLabel.widthAnchor.constraint(equalToConstant: 100).isActive = true
        statusLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 40).isActive = true
        
        dateLabel.topAnchor.constraint(equalTo: statusButton.topAnchor).isActive = true
        dateLabel.leadingAnchor.constraint(equalTo: cardView.leadingAnchor, constant: 10).isActive = true
        dateLabel.trailingAnchor.constraint(equalTo: statusButton.leadingAnchor, constant: -2).isActive = true
        dateLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        leaveLogoImageView.topAnchor.constraint(equalTo: dateLabel.bottomAnchor,constant:10).isActive = true
        leaveLogoImageView.leadingAnchor.constraint(equalTo: dateLabel.leadingAnchor).isActive = true
        leaveLogoImageView.widthAnchor.constraint(equalToConstant: 16).isActive = true
        leaveLogoImageView.heightAnchor.constraint(equalToConstant: 16).isActive = true
        
        titleLabel.topAnchor.constraint(equalTo: leaveLogoImageView.topAnchor,constant:-5).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: leaveLogoImageView.trailingAnchor,constant:8).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: dateLabel.trailingAnchor).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: 25).isActive = true
    }
    
    func shadowforView(button:UIView,color:UIColor){
        
        button.layer.shadowColor = color.cgColor
        //    button.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        button.layer.shadowOffset = CGSize(width: 0, height: 3)
        button.layer.shadowOpacity = 1.0
        button.layer.shadowRadius = 10.0
        button.layer.masksToBounds = false
        
    }
    


}
