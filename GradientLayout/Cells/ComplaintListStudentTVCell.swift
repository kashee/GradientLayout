//
//  ComplaintListStudentTVCell.swift
//  GradientLayout
//
//  Created by kashee on 09/08/18.
//  Copyright © 2018 Pro Retina. All rights reserved.
//

import UIKit

class RoundedLabel: UILabel {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let maskPath = UIBezierPath(roundedRect: bounds,
                                    byRoundingCorners: [.topLeft, .bottomRight],
                                    cornerRadii: CGSize(width: 5, height: 5))
        
        let maskLayer = CAShapeLayer()
        maskLayer.frame = bounds
        maskLayer.cornerRadius = 5
        maskLayer.masksToBounds = true
        maskLayer.path = maskPath.cgPath
        layer.mask = maskLayer
    }
}

class ComplaintListStudentTVCell: UITableViewCell {

    let cardView:UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.init(red: 255.0 / 255.0, green: 255.0 / 255.0, blue: 255.0 / 255.0, alpha: 1.0)
        view.layer.cornerRadius = 5
        view.clipsToBounds = true
//        view.layer.masksToBounds = false
        return view
    }()
    
    let priorityLabel:RoundedLabel = {
        let label = RoundedLabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Critical"
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textColor = UIColor.white
//        label.layer.masksToBounds = true
        label.backgroundColor = UIColor(red: 250/255, green: 82/255, blue: 90/255, alpha: 0.81)
        return label
    }()
    
//    var priorityLabel = RoundedLabel()
    
//    priorityLabel.te
    
    let titleLabe:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Tab Leakage"
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.textColor = UIColor(red: 33.0/255.0, green: 73.0/255.0, blue: 88.0/255.0, alpha: 1)
        return label
    }()
    
    let dateLabe:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "2.06 pm, Today"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textColor = UIColor(red: 33.0/255.0, green: 73.0/255.0, blue: 88.0/255.0, alpha: 1)
        return label
    }()
    
    let leaveLogoImageView:UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "icons8Hospital48"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        //        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let LocationLabel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Room 201, Block A"
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.60)
        return label
    }()
    
    let descriptionLabel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "This is the tap leakage description by the student. This can also go to two lines."
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.numberOfLines = 0
        label.sizeToFit()
        label.textColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.60)
        return label
    }()
    
    let statusLabel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "In Progress"
        label.textColor = .black
        label.textAlignment = .right
        label.numberOfLines = 0
        label.sizeToFit()
        label.font = UIFont.boldSystemFont(ofSize: 11)
        label.textColor = UIColor(red: 245/255.0, green: 166/255.0, blue: 35/255.0, alpha: 0.75)
        return label
    }()
    
    let statusButton:UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(#imageLiteral(resourceName: "noun1763918Cc-1"), for: .normal)
        button.imageView?.contentMode = .scaleAspectFit
        button.contentVerticalAlignment = .fill
        button.contentHorizontalAlignment = .fill
        button.imageEdgeInsets = UIEdgeInsetsMake(7, 10, 7, 10)
        //        button.imageEdgeInsets = UIEdgeInsetsMake(2, 5, 2, 5)
        button.backgroundColor = UIColor(red: 245/255.0, green: 130/255.0, blue: 35/255.0, alpha: 1)
        button.layer.cornerRadius = 14
        return button
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = UIColor(red: 10/255, green: 178/255, blue: 230/255, alpha: 0.0)
        addAutoLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        shadowforView(button: cardView, color: UIColor(red: 37/255, green: 38/255, blue: 94/255, alpha: 0.1))
    }
    
    func addAutoLayout(){
        self.addSubview(cardView)
        cardView.addSubview(priorityLabel)
        cardView.addSubview(titleLabe)
        cardView.addSubview(LocationLabel)
        cardView.addSubview(statusLabel)
        cardView.addSubview(statusButton)
        cardView.addSubview(leaveLogoImageView)
        
        cardView.addSubview(LocationLabel)
        cardView.addSubview(descriptionLabel)
        cardView.addSubview(dateLabe)
        
        
        cardView.topAnchor.constraint(equalTo: self.topAnchor,constant:10).isActive = true
        cardView.leadingAnchor.constraint(equalTo: self.leadingAnchor,constant:10).isActive = true
        cardView.trailingAnchor.constraint(equalTo: self.trailingAnchor,constant:-10).isActive = true
        cardView.bottomAnchor.constraint(equalTo: self.bottomAnchor,constant:-10).isActive = true
        
        
        priorityLabel.topAnchor.constraint(equalTo: cardView.topAnchor).isActive = true
        priorityLabel.leadingAnchor.constraint(equalTo: cardView.leadingAnchor).isActive = true
        priorityLabel.widthAnchor.constraint(equalToConstant: 70).isActive = true
        priorityLabel.heightAnchor.constraint(equalToConstant: 25).isActive = true
//        priorityLabel.backgroundColor = .red
//        priorityLabel.trailingAnchor.constraint(equalTo: contentview.trailingAnchor).isActive = true
        
        statusButton.topAnchor.constraint(equalTo: priorityLabel.bottomAnchor, constant: 10).isActive = true
        statusButton.trailingAnchor.constraint(equalTo: cardView.trailingAnchor, constant: -12).isActive = true
        statusButton.widthAnchor.constraint(equalToConstant: 42).isActive = true
        statusButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        statusLabel.topAnchor.constraint(equalTo: statusButton.bottomAnchor, constant: 5).isActive = true
        statusLabel.trailingAnchor.constraint(equalTo: statusButton.trailingAnchor).isActive = true
        statusLabel.widthAnchor.constraint(equalToConstant: 100).isActive = true
        statusLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 20).isActive = true
        
        
        leaveLogoImageView.topAnchor.constraint(equalTo: statusButton.topAnchor).isActive = true
        leaveLogoImageView.leadingAnchor.constraint(equalTo:  cardView.leadingAnchor, constant: 10).isActive = true
        leaveLogoImageView.widthAnchor.constraint(equalToConstant: 48).isActive = true
        leaveLogoImageView.heightAnchor.constraint(equalToConstant: 48).isActive = true
        
        titleLabe.topAnchor.constraint(equalTo: leaveLogoImageView.topAnchor).isActive = true
        titleLabe.leadingAnchor.constraint(equalTo: leaveLogoImageView.trailingAnchor, constant: 8).isActive = true
        titleLabe.trailingAnchor.constraint(equalTo: statusButton.leadingAnchor, constant: -2).isActive = true
        titleLabe.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        dateLabe.topAnchor.constraint(equalTo: titleLabe.bottomAnchor, constant: 2).isActive = true
        dateLabe.leadingAnchor.constraint(equalTo: titleLabe.leadingAnchor).isActive = true
        dateLabe.trailingAnchor.constraint(equalTo: statusLabel.leadingAnchor,constant:-5).isActive = true
        dateLabe.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        LocationLabel.topAnchor.constraint(equalTo: leaveLogoImageView.bottomAnchor,constant:20).isActive = true
        LocationLabel.leadingAnchor.constraint(equalTo: leaveLogoImageView.leadingAnchor).isActive = true
        LocationLabel.trailingAnchor.constraint(equalTo: cardView.trailingAnchor,constant:-10).isActive = true
        LocationLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        descriptionLabel.topAnchor.constraint(equalTo: LocationLabel.bottomAnchor).isActive = true
        descriptionLabel.leadingAnchor.constraint(equalTo: LocationLabel.leadingAnchor).isActive = true
        descriptionLabel.trailingAnchor.constraint(equalTo: LocationLabel.trailingAnchor).isActive = true
        descriptionLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
    
    func shadowforView(button:UIView,color:UIColor){
        
        button.layer.shadowColor = color.cgColor
        button.layer.shadowOffset = CGSize(width: 0, height: 3)
        button.layer.shadowOpacity = 1.0
        button.layer.shadowRadius = 10.0
        button.layer.masksToBounds = false
        
    }

}
